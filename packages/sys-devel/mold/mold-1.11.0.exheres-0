# Copyright 2021 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=rui314 tag=v${PV} ] alternatives cmake [ ninja=true ]

SUMMARY="A Modern Linker"
DESCRIPTION="
mold is a high-performance drop-in replacement for existing Unix linkers. It is several times faster
than LLVM lld linker, the (then-) fastest open-source linker which I originally created a few years
ago. Here is a performance comparison of GNU gold, LLVM lld, and mold for linking final executables
of major large programs.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# mold is a cross linker and can compile for any target without a
# new binary being compiled for the target. this is only used for the
# creation of the symlinks for targets; /usr/${CHOST}/bin/${target}-mold
CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    i686-pc-linux-gnu
    i686-pc-linux-musl
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS+="
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

DEPENDENCIES="
    build+run:
        app-arch/zstd
        dev-libs/openssl:= [[ note = [ for libcrypto ] ]]
        dev-libs/xxHash
        sys-libs/zlib
"

# Use of unprefixed tools
RESTRICT="test"

mold_target() {
    case "${1}" in
        aarch64-*)
            echo "aarch64linux"
            ;;
        i686-*)
            echo "elf_i386"
            ;;
        x86_64-*)
            echo "elf_x86_64"
            ;;
    esac
}

src_install() {
    cmake_src_install

    # ban ld.mold, alternative for banned ld
    dobanned ld.mold

    local alternatives=(
        /usr/$(exhost --target)/bin/ld ld.mold
        "${BANNEDDIR}"/ld              ld.mold
    )

    # provide host-prefixed ld.mold
    for target in ${CROSS_COMPILE_TARGETS}; do
        if option targets:${target}; then
            herebin ${target}-ld.mold << EOF
#!/usr/bin/env sh
exec /usr/host/bin/ld.mold -m $(mold_target ${target}) "\${@}"
EOF

            alternatives+=( /usr/$(exhost --target)/bin/${target}-ld ${target}-ld.mold )
        fi
    done

    alternatives_for ld mold 5 "${alternatives[@]}"
}

